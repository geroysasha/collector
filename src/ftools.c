#include <unistd.h>
#include <fcntl.h>

#include <sys/file.h>
#include <sys/mman.h>

#include "ftools.h"
#include "types.h"

int	ft_open_file(const unsigned char* const fname, int mode)
{
	return open(fname, mode, 0777);
}

int ft_close_file(int fd)
{
	lockf(fd, F_LOCK, 0);
	
	const int res = close(fd);
	
	lockf(fd, F_ULOCK, 0);	
	
	return res;
}

int	ft_write_file(int fd, const unsigned char* const buffer, int size)
{
	lockf(fd, F_LOCK, 0);
		
	const int res = write(fd, buffer, size);

	lockf(fd, F_ULOCK, 0);
	
    return res;
}

int	ft_read_file(int fd, unsigned char* buffer, int size)
{
	lockf(fd, F_LOCK, 0);
		
	const int res = read(fd, buffer, size);

	lockf(fd, F_ULOCK, 0);
	
    return res;
}

long ft_get_filesize(int fd)
{
	lockf(fd, F_LOCK, 0);
	
	const int len = lseek(fd, 0, SEEK_END);

	lseek (fd, 0, SEEK_SET);
	
	lockf(fd, F_ULOCK, 0);	
	
	return len;
}

void* ft_create_shm_file(const unsigned char* const fname)
{
	int fd = shm_open(fname, O_CREAT | O_TRUNC | O_RDWR, 0777);
	if (fd == -1)
	  return NULL;
	
	int r = ftruncate(fd, sysconf(_SC_PAGE_SIZE));
	if (r != 0)
		return NULL;
	
	void *ptr = mmap(0, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED)
		return NULL;
		
	close(fd);	
	
	return ptr;
}

void* ft_get_shm_file(const unsigned char* const fname)
{
	int fd = shm_open(fname, O_TRUNC | O_RDWR, 0777);
	if (fd == -1)
	  return NULL;
	
	int r = ftruncate(fd, sysconf(_SC_PAGE_SIZE));
	if (r != 0)
		return NULL;
	
	void *ptr = mmap(0, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED)
		return NULL;
	
	close(fd);	
	
	return ptr;
}

int ft_delete_shm_file(const unsigned char* const fname)
{
	return shm_unlink(fname);
}















