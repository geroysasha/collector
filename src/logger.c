#include "logger.h"
#include "ftools.h"
#include "types.h"

static const unsigned char* const log_file = "log.txt";

static int fd = -1;

int lgr_init()
{
	fd = ft_open_file(log_file, FILE_RDWR | FILE_CREAT | FILE_TRUNC);
	
    if(fd < 0)
        return FILED_OPEN_LOG_FILE;
	
	return SUCCESS;
}

int lgr_add_info(const unsigned char* const buffer, int size)
{
	const int res = ft_write_file(fd, buffer, size);
	
	if(res != size)
		return FILED_WRITE_FILE;
	
	return SUCCESS;
}