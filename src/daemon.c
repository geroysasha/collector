#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

#include "daemon.h"
#include "sniffer.h"
#include "ftools.h"
#include "types.h"

//#define SIGUSR3 (SIGRTMIN +4)
//#define SIGUSR4 (SIGRTMIN +5)

static int  save_pid();
static int  main_loop();
static void signal_handler(int sig);
static void read_pid();

static const unsigned char* const shared_file_name = "/collector";
static const unsigned char* const pid_file = "collector.pid";

static void* shared_file = NULL;
static pid_t pid = INVALID_PID;

static volatile sig_atomic_t stat = RUN_DAEMON;

static const unsigned char* const STATUS_OK = "OK";
static const unsigned char* const STATUS_ERR = "ERROR";

int dmn_start()
{
	//does pid exist
    if(dmn_get_pid() > 0)
        return DAEMON_EXIST;

	//attempt to initialize sniffer
    const int status = snf_init();
    if(status != SUCCESS)
        return status;
    
	//attempt to create shared memory object
    shared_file = ft_create_shm_file(shared_file_name);
    if(shared_file == (char*)-1)
        return FILED_CREATE_SHR_FILE; 
    
    pid = fork();
    if (pid < 0) 
    {
        return FILED_CREATE_DAEMON;
    }
    else if(pid > 0)
    {
		//stop process if could not save its pid
        if(save_pid() == FILED_SAVE_PID)
        {
            dmn_stop();
            return FILED_CREATE_DAEMON;
        }
       
        return SUCCESS_CREAT_DAEMON;
    }     
   
    signal(SIGUSR1, signal_handler);
    signal(SIGUSR2, signal_handler);
    //signal(SIGUSR3, signal_handler);
    //signal(SIGUSR4, signal_handler);

    main_loop();

    return DAEMON_FINISHED;
}

int dmn_stop()
{
    if(kill(dmn_get_pid(), SIGUSR1) != SUCCESS_KILL)
        return FILED_KILL;

    return SUCCESS_STOPPED;
}

int dmn_select(const unsigned char* const iface)
{
	//get pointer to shared memory object
    shared_file = ft_get_shm_file(shared_file_name);
	
	//assign iface
    strcpy(shared_file, iface);

    if(kill(dmn_get_pid(), SIGUSR2) != SUCCESS_KILL)
        return FILED_KILL;
    
	//busy wait response status
    while(strcmp(STATUS_ERR,(char*) shared_file) && strcmp(STATUS_OK, (char*) shared_file))
    {
        sleep(1);
    }
    
    if(strcmp(STATUS_ERR, (char*) shared_file) == 0)
        return FILED;
    
    return SUCCESS;
}

long dmn_get_pid()
{
    if(pid < 0)
    {
        read_pid();        
    }
        
    return pid;
}

void read_pid()
{
    const int fd_pid = ft_open_file(pid_file, FILE_RDONLY);
    if(fd_pid < 0)
        return;
        
    char pid_buf[sizeof(long)];
    ft_read_file(fd_pid, pid_buf, sizeof(pid_buf));
    pid = atoi(pid_buf);
    
    ft_close_file(fd_pid);
    
    //check is PID exist
    if (kill(pid, 0) != 0)
        pid = INVALID_PID;
}

int save_pid()
{
    const int fd_pid = ft_open_file(pid_file, FILE_RDWR | FILE_CREAT);
    if(fd_pid < 0)
        return FILED_SAVE_PID;
    
	char pid_buf[sizeof(long)];
	sprintf(pid_buf, "%ld", (long)pid);        
   
    int status = SUCCESS_SAVE_PID;
    if(ft_write_file(fd_pid, pid_buf, sizeof(long)) == 0)
    {
        status = FILED_SAVE_PID;
    }
    
    ft_close_file(fd_pid);
    
    return status;
}

int main_loop()
{
    while (stat == RUN_DAEMON) 
    {
        snf_scan();
    }
}

void signal_handler(int sig) 
{   
    if(sig == SIGUSR1)
    {
		//sniffer destroy
        snf_destroy();
		
		//delete shared memory object
        ft_delete_shm_file(shared_file_name);
        
		//quit from main loop
        stat = QUITE_DAEMON;
    }
    else if(sig == SIGUSR2)
    {
        const unsigned char* const iface = (const unsigned char* const) shared_file;
        const unsigned char* status = STATUS_OK;
        
		//attempt to change iface
        if(snf_change_iface(iface) < 0)
            status = STATUS_ERR;
        
        strcpy(shared_file, status);
    }
}











