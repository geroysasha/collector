#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netinet/ip.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>

#include <regex.h>   

#include "sniffer.h"
#include "logger.h"
#include "types.h"

static void save_ip(const unsigned char* const buffer, int size);
static void get_default_ethernet();

static const unsigned char* const open_socket_error = "Open socket error.\n";
static const unsigned char* const recv_error = "Recvfrom error.\n";
static const unsigned char* const delim_ip = "\n";
static const unsigned char* const delim_iface = "@";

static const int		SIZE = 1024;
static unsigned char	iface_default[16];
static unsigned char*	buffer = NULL;

static int		sock;
static struct	sockaddr saddr;
static struct	sockaddr_in source, dest;

static regex_t	regex;
static int		regres;

int snf_init()
{
	sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if(sock < 0)
    {
        lgr_add_info(open_socket_error, strlen(open_socket_error));
        return FILED_OPEN_SOCKET;
    }
	
	if(lgr_init() != SUCCESS)
		return FILED_LOGGER_INIT;
	
	buffer = (unsigned char*) malloc(SIZE * sizeof(unsigned char*));
	
	get_default_ethernet();

	regres = regcomp(&regex, 
				"^([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
				 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
				 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
				 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$", REG_EXTENDED);	
	
	if (regres)
		return FILED_OPEN_SOCKET;
	
	return SUCCESS;
}

void snf_scan()
{
	const int saddr_size = sizeof(saddr);
	const int data_size = recvfrom(sock, buffer, SIZE, 0, &saddr, (socklen_t*)&saddr_size);
	if(data_size < 0 )
	{
		lgr_add_info(recv_error, strlen(recv_error));
	}
	else
	{
		save_ip(buffer, data_size);	
	}
}

int snf_destroy()
{
	regfree(&regex);
	shutdown(sock, SHUT_RDWR);
	close(sock);
	free(buffer);
	
	return SUCCESS;
}

int snf_change_iface(const unsigned char* const iface)
{
	shutdown(sock, SHUT_RDWR);
	int stat = setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface) + 1);//ens33
	if(stat == 0)
	{
		lgr_add_info(delim_iface, strlen(delim_iface));
		lgr_add_info(iface, strlen(iface));
		lgr_add_info(delim_ip, strlen(delim_ip));
	}
	
	return stat;
	
}

void save_ip(const unsigned char* const buffer, int size)
{
    unsigned short iphdrlen;
         
    struct iphdr *iph = (struct iphdr *)(buffer  + sizeof(struct ethhdr) );
    iphdrlen = iph->ihl*4;
     
    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = iph->saddr;

    const unsigned char* const ip = inet_ntoa(source.sin_addr);
	
	if (!regexec(&regex, ip, 0, NULL, 0)) 
	{
		lgr_add_info(ip, strlen(ip));
		lgr_add_info(delim_ip, strlen(delim_ip));
	}
}

void get_default_ethernet()
{
	struct ifaddrs *ifaddr, *ifa;
	unsigned char host[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1) 
		return;

	int family, n;
	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) 
	{
	   if (ifa->ifa_addr == NULL)
		   continue;

	   family = ifa->ifa_addr->sa_family;
		
		if(strcmp(ifa->ifa_name,"lo"))
		{
			strcpy(iface_default, ifa->ifa_name);
			
			lgr_add_info(delim_iface, strlen(delim_iface));			
			lgr_add_info(iface_default, strlen(iface_default));
			lgr_add_info(delim_ip, strlen(delim_ip));
			break;
		}
	}

	freeifaddrs(ifaddr);	
}




