#include <stdio.h>
#include <string.h>

#include "daemon.h"
#include "parser.h"
#include "types.h"

static const unsigned char* const log_file = "log.txt";

static int cmd_handler(int argc, char *argv[]);

static unsigned char* app_name = NULL;

static const struct cmd_t
{
	char* START;
	char* STOP;
	char* SHOW;
	char* SELECT;
	char* STAT;
	char* HELP;
} CMD = {"--start", "--stop", "--show", "--select", "--stat", "--help"};

void print_help()
{
	printf("\n Usage: %s [OPTIONS]\n\n", app_name);
	printf("  Options:\n");
	printf("   --help                   Print this help\n");
	printf("   --start                  Packets are being sniffed from now on from default iface(eth0)\n");
	printf("   --stop                   Packets are not sniffed\n");
	printf("   --show [ip] count        Print number of packets received from ip address\n");
	printf("   --select iface [iface]   Select interface for sniffing eth0, wlan0, ethN, wlanN...\n");
	printf("   --stat [iface]           Show all collected statistics for particular interface,\n");
	printf("                            if iface omitted - for all interfaces\n");
	printf("\n");
}

int main(int argc, char *argv[])
{
	app_name = argv[0];
	
	if(argc > 1)
	{		
		switch(cmd_handler(argc, argv))
		{
			case SUCCESS_CREAT_DAEMON:
				printf("Collector has pid:%ld\n", dmn_get_pid());
				printf("Collector was started.\n");
				break;
			case SUCCESS_STOPPED:
				printf("Collector was stopped.\n");
				break;
			case SUCCESS:
				printf("Success.\n");
				break;				
			case FILED_KILL:
				printf("Kill event was unsuccessful.\n");
				break;
			case FILED_LOGGER_INIT:
				printf("Logger could not be initialized.\n");
				break;
			case FILED_OPEN_SOCKET:
				printf("Socket could not be opened.\n");
				break;
			case FILED_SETSOCKOPT:
				printf("Setsockopt return -1.\n");
				break;					
			case FILED:
				printf("Failed\n");
				break;
			case FILED_CREATE_DAEMON:
				printf("Filed create daemon.\n");
				break;
			case DAEMON_EXIST:
				printf("Collector exist.\n");
				break;				
			case DAEMON_NOT_EXIST:
				printf("Collector doesn't exist\n");
				break;				
			case DAEMON_FINISHED:
				break;					
			default:
				printf("You should use %s --help for more information\n", app_name);
				break;
		}
	}
	else
	{
		print_help();
	}
	
	return 0;
}

int cmd_handler(int argc, char *argv[])
{
	if(!strcmp(argv[1], CMD.HELP)) 
	{		
		print_help();
		
		return SUCCESS;
	}	
	else if(!strcmp(argv[1], CMD.START)) 
	{		
		return dmn_start();
	}
	else if(!strcmp(argv[1], CMD.STOP))
	{
		return dmn_stop();
	}
	else if(!strcmp(argv[1], CMD.SHOW))
	{
		if(argc >= 4 && strcmp(argv[3], "count") == 0)
		{
			const int status = prs_data_prepare(log_file);
			if(status == SUCCESS)
			{
				const int size = prs_part_statistic(argv[2]);
				printf("IP:	%s	|	%d\n", argv[2], size);
				return SUCCESS;
			}

			prs_data_free();				
			return status;
		}
		
		return FILED;
	}
	else if(!strcmp(argv[1], CMD.SELECT))
	{		
		if(argc >= 4 && strcmp(argv[2], "iface") == 0)
		{
			printf("Try to set iface = %s \n", argv[3]);
			return dmn_select(argv[3]);
		}
		
		return FILED;
	}
	else if(!strcmp(argv[1], CMD.STAT))
	{
		const int status = prs_data_prepare(log_file);
		if(status != SUCCESS)
			return status;
		
		unsigned char* iface = NULL;
		if(argc >= 3)
		{
			iface = argv[2];
		}
		
		unsigned char** ip = NULL;
		int* num = NULL;
		const int size = prs_statistic(iface, &ip, &num);

		if(size != 0)
		{
			int j = 0;	
			for(int i = 0; i < size; ++i)
			{
				printf("	%15s	|	%d\n", ip[j], num[i]);			
				j += num[i];
			}
		}
		prs_data_free();
		return SUCCESS;
	}
	
	return UNKNOWN;
}
































