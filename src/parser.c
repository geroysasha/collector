#include <stdlib.h>
#include <string.h>
//#include <stdio.h>

#include "parser.h"
#include "ftools.h"
#include "types.h"

static unsigned char*	data		= NULL;
static unsigned char*	data_part	= NULL;
static unsigned char**	array_ip	= NULL;
static unsigned char**	array_iface	= NULL;
static int* 			array_num	= NULL;

static const unsigned char* const  delim_ip = "\n";
static const unsigned char* const  delim_iface = "@";

static	int             count(const unsigned char* const source, const unsigned char* const delim);
static	int             split(unsigned char* const  source, unsigned char** result, const unsigned char* const delim, int size);
static	int             assemble(unsigned char* const source, unsigned char*** ip, int** num, int size);
static  unsigned char*  prepare_iface_data(const unsigned char* const iface);
    
int prs_data_prepare(const unsigned char* const file_name)
{
    if(file_name == NULL)
        return FILED_OPEN_FILE;
    
    int fd = ft_open_file(file_name, FILE_RDONLY);
    if(fd < 0)
        return FILED_OPEN_FILE;

	const long len = ft_get_filesize(fd);
	data = (unsigned char*) malloc(len * sizeof(unsigned char*));
	
	//read file with necessary information
	ft_read_file(fd, data, len);
	
    ft_close_file(fd);

	return SUCCESS;
}

int prs_data_free()
{
	free(data);	
	free(data_part);	
	free(array_ip);
	free(array_iface);
	free(array_num);
	
	return SUCCESS;
}

int prs_part_statistic(const unsigned char* const ip)
{
    if(ip == NULL)
        return 0;
    
	//get number of ip
	const int size = count(data, delim_ip);
    
    if(size == 0)
        return 0;
    
	array_ip = (unsigned char**) malloc(size * sizeof(unsigned char**));
	
	//create array of ip
	if(split(data, array_ip, delim_ip, size) == 0)
		return 0;	
    
	//count the identical ip
    int count = 0;
	for(int i = 0; i < size; ++i)
	{
		if(strcmp(ip, array_ip[i]) == 0)
		{
			++count;
		}
	}	
	
	return count;
}

int prs_statistic(const unsigned char* const iface, unsigned char*** ip, int** num)
{   
    unsigned char* info = NULL;
    if(iface != NULL)
    {
        info = prepare_iface_data(iface);
    }
    else
    {
        info = data;
    }

	//get number of ip
	const int size_ip = count(info, delim_ip); 
	array_ip = (unsigned char**) malloc(size_ip * sizeof(unsigned char**));
	array_num = (int*) malloc(size_ip * sizeof(int*));
	
	//assemble statistical information
    const int count = assemble(info, &array_ip, &array_num, size_ip);
	
    if(count != 0)
    {
        *ip = array_ip;
        *num = array_num;
    }
    
	return count;
}

unsigned char* prepare_iface_data(const unsigned char* const iface)
{
	//get number of iface
	const int size = count(data, delim_iface);
    
    if(size == 0)
        return 0;
    
	array_iface = (unsigned char**) malloc(size * sizeof(unsigned char**));
	if(array_iface == NULL)
		return 0;
	
	//create array of iface
	if(split(data, array_iface, delim_iface, size) == 0)
		return 0;
	
	//len_all_iface - contains size for future data array
	long len_all_iface = 0;
	
	//count size
	for(int i = 0; i < size; ++i)
	{
		//look for iface
		int j = 0;
		while(array_iface[i][j] != *delim_ip && array_iface[i][j] == iface[j])
		{
			++j;
		}
		
		//is not correct iface
		if(j != strlen(iface))
        {
            array_iface[i] = NULL;
        }
		else
        {
            len_all_iface += (strlen(array_iface[i]) - 1);
        }
	}

	data_part = (unsigned char*) malloc(len_all_iface * sizeof(unsigned char*));
    
	int j = 0;
	for(int i = 0; i < size; ++i)
	{
		//is not correct iface
		if(array_iface[i] == NULL)
			continue;
		
		//when correct iface, copy part of full data to new array
		memcpy(data_part + j, array_iface[i], strlen(array_iface[i]));
		
		//shift position
		j += strlen(array_iface[i]);
	}  
    
    return data_part;
}

int assemble(unsigned char* const source, unsigned char*** ip, int** num, int size)
{
	if(source == NULL || ip == NULL || num == NULL || size == 0)
		return 0;

    unsigned char** array_ip = *ip;
    int* array_num = *num;
    
	//create array of ip
	if(split(source, array_ip, delim_ip, size) == 0)
		return 0;
   
   	int count = 0;
	int cur_pos = 0;
	
	//sort array of ip
	//count number the similar ip
	for(int j = 0; j < size - 1;)
	{
		for(int k = j + 1; k < size; ++k)
		{
			if(strcmp(array_ip[j], array_ip[k]) == 0)
			{
				if((cur_pos + 1) >= size)
					break;

				//exchange position ip
				unsigned char* tmp = array_ip[cur_pos + 1];
				array_ip[cur_pos + 1] = array_ip[k];
				array_ip[k] = tmp;
				++cur_pos;
			}
		}
		
		++cur_pos;
		
		//save number the similar ip
		array_num[count] = cur_pos - j;	
		
		++count;
		
		j = cur_pos;
	}   
    return count;
}

int count(const unsigned char* const source, const unsigned char* const delim)
{
    if(source == NULL || delim == NULL)
        return 0;
    
	const long len = strlen(source);
	int count = 0;
	for(int i = 0; i < len; ++i)
	{
		if(source[i] == *delim)
        {
            ++count;
        }
	}
	
	return count;
}

int split(unsigned char* const source, unsigned char** result, const unsigned char* const delim, int size)
{
    if(source == NULL || result == NULL || delim == NULL || size == 0)
        return 0;
        
	long count = 0;	
    unsigned char* char_pointer = strtok(source, delim);
    while(char_pointer)
	{       
        if(count >= size)
            break;
        
		result[count] = char_pointer;
        char_pointer = strtok(NULL, delim);
		++count;
	}

	return count;
}