target		:= collector
compiler	:=	gcc
source_dirs	:=	src
include_dir	:=	inc
output_dir	:=	obj
lib			:=	-lrt

source_files	:=	$(wildcard   $(addsuffix /*.c, $(source_dirs) ) )

object_files	:=	$(notdir $(source_files))
object_files	:=	$(notdir $(object_files:.c=.o))
object_files	:=	$(patsubst %,$(output_dir)/%,$(object_files))

$(target): $(object_files)
	$(compiler) -o $@ $^ $(lib)
	
$(output_dir)/%.o: $(source_dirs)/%.c
	mkdir -p $(output_dir)
	$(compiler) -c $< $(addprefix -I, $(include_dir)) -o $@

clean:
	rm -f $(output_dir)/*.o $(target)
	rmdir $(output_dir)