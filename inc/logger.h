#ifndef PRJ_LOGGER_H
#define PRJ_LOGGER_H

//logger initialization
int lgr_init();

//send to log file
int lgr_add_info(const unsigned char* const buffer, int size);

#endif