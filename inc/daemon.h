#ifndef PRJ_DAEMON_H
#define PRJ_DAEMON_H

// create and start daemon
int		dmn_start();

//stop daemon
int		dmn_stop();

//select iface
int		dmn_select(const unsigned char* const iface);

//get daemon's pid
long	dmn_get_pid();

#endif