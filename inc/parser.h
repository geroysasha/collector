#ifndef PRJ_PARSER_H
#define PRJ_PARSER_H

//prepare the data for getting statistic
int prs_data_prepare(const unsigned char* const file_name);

//clean resources
int prs_data_free();

//get statistic for particular ip
int prs_part_statistic(const unsigned char* const ip);

//get statistic for particular iface	
int prs_statistic(const unsigned char* const iface, unsigned char*** ip, int** num);

#endif