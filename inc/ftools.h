#ifndef PRJ_FTOOLS_H
#define PRJ_FTOOLS_H

int		ft_open_file(const unsigned char* const fname, int mode);

int		ft_close_file(int fd);

int		ft_write_file(int fd, const unsigned char* const buffer, int size);

int		ft_read_file(int fd, unsigned char* buffer, int size);

long	ft_get_filesize(int fd);

//create shared memory object
void*	ft_create_shm_file(const unsigned char* const fname);

//get shared memory object
void*	ft_get_shm_file(const unsigned char* const fname);

//delete shared memory object
int		ft_delete_shm_file(const unsigned char* const fname);

#endif