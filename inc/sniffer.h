#ifndef PRJ_SNIFFER_H
#define PRJ_SNIFFER_H

//sniffer initialization
int		snf_init();

//scan socket and save ip to log file
void	snf_scan();

//change iface
int		snf_change_iface(const unsigned char* const iface);

//sniffer destroy
int		snf_destroy();

#endif